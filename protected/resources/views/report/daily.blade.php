@extends('template')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Daily Report : <b class="bulanReport">{{ $day }}  {{ $month }} {{ $year }}</b>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daily Report Table</h3>
              &nbsp;
              <small>
                  <a href="javascript:void(0)" class="exportExcel" data-title="DailyReport_{{ $day }}_{{ $month }}_{{ $year }}">
                      <i class="fa fa-print"></i> Export to Excel
                  </a>
              </small>
              <div class="box-tools">
                <div style="width: 250px;" class="input-group input-group-sm">
                  <input type="text" placeholder="Pilih Tanggal Disini" class="form-control pull-right dateOnly" />
                  <div class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding ajax_report_field">
              <table class="table table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>User</th>
                        <th>Tipe Perangkat</th>
                        <th>Nama Perangkat</th>
                        <th>Tanggal Peminjaman</th>
                        <th>Tujuan</th>
                        <th>Lama Peminjaman</th>
                        <th>Tanggal Pengembalian</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <?php $no=1; ?>
                @if(!empty($data))
                <tbody>
                    @foreach($data as $v)
                        <tr>
                            <td>
                                {{ $no++ }}
                            </td>
                            <td>
                                {{ $v->user->name }}
                            </td>
                            <td>
                                {{ ucwords(str_replace("-"," ",$v->tipe_perangkat)) }}
                            </td>
                            <td>
                                {{ $v->barang->nama_barang }}
                            </td>
                            <td>
                                {{ $v->tanggal }}
                            </td>
                            <td>
                                {{ $v->tujuan }}
                            </td>
                            <td>
                               {{$v->lama_pinjaman}} Hari
                            </td>
                            <td>
                               {{substr($v->due_date,0,10)}}
                            </td>
                            <td>
                                @if($v->status == 0)
                                    <span class="label label-success">
                                         Returned
                                    </span>
                                @else
                                    @if($status[$v->id]['status'] == false)
                                        <span class="label label-danger">
                                            {{ $status[$v->id]['msg'] }}
                                        </span>
                                    @elseif($status[$v->id]['status'] === 'today')
                                        <span class="label label-warning">
                                            {{ $status[$v->id]['msg'] }}
                                        </span>
                                    @else
                                        <span class="label label-success">
                                            {{ $status[$v->id]['msg'] }}
                                        </span>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                @endif
              </table>
            </div>
            <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection


@section('modal')
    @include('modal_confirm',['modal_title'=>'Barang'])
@endsection