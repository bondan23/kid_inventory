<table class="table table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>User</th>
            <th>Tipe Perangkat</th>
            <th>Nama Perangkat</th>
            <th>Tanggal Peminjaman</th>
            <th>Tujuan</th>
            <th>Lama Peminjaman</th>
            <th>Tanggal Pengembalian</th>
            <th>Status</th>
        </tr>
    </thead>
    <?php $no=1; ?>
    @if(!empty($data))
    <tbody>
        @foreach($data as $v)
            <tr>
                <td>
                    {{ $no++ }}
                </td>
                <td>
                    {{ $v->user->name }}
                </td>
                <td>
                    {{ ucwords(str_replace("-"," ",$v->tipe_perangkat)) }}
                </td>
                <td>
                    {{ $v->barang->nama_barang }}
                </td>
                <td>
                    {{ $v->tanggal }}
                </td>
                <td>
                    {{ $v->tujuan }}
                </td>
                <td>
                   {{$v->lama_pinjaman}} Hari
                </td>
                <td>
                   {{substr($v->due_date,0,10)}}
                </td>
                <td>
                    @if($v->status == 0)
                        <span class="label label-success">
                             Returned
                        </span>
                    @else
                        @if($status[$v->id]['status'] == false)
                            <span class="label label-danger">
                                {{ $status[$v->id]['msg'] }}
                            </span>
                        @elseif($status[$v->id]['status'] === 'today')
                            <span class="label label-warning">
                                {{ $status[$v->id]['msg'] }}
                            </span>
                        @else
                            <span class="label label-success">
                                {{ $status[$v->id]['msg'] }}
                            </span>
                        @endif
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
    @endif
    </table>