@extends('template')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Permintaan Verifikasi
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Table Permintaan Verifikasi</h3>

              <div class="box-tools">
                <div style="width: 150px;" class="input-group input-group-sm">
                  <input type="text" placeholder="Search" class="form-control pull-right" name="table_search">

                  <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                 <th></th>
                  <th>No</th>
                  <th>User</th>
                  <th>Tipe Perangkat</th>
                  <th>Nama Perangkat</th>
                  <th>Tanggal Peminjaman</th>
                  <th>Tujuan</th>
                  <th>Lama Peminjaman</th>
                  <th>Status</th>
                </tr>
                <?php $no=1; ?>
                @foreach($data as $v)
                <tr>
                  <td>
                      {{ Form::checkbox('cek') }}
                  </td>
                  <td>{{ $no++ }}</td>
                  <td>{{ $v->nama }}</td>
                  <td>{{ $v->tipe_perangkat }}</td>
                  <td>{{ $v->nama_perangkat }}</td>
                  <td>{{ $v->tanggal }}</td>
                  <td>
                      {{$v->tujuan}}
                  </td>
                  <td>
                      {{$v->lama_pinjaman}} Hari
                  </td>
                  <td>
                     @if($v->status == 0)
                      <span class="label label-warning">Pending</span>
                     @else
                      <span class="label label-success">Approve</span>
                     @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button class="btn btn-primary" type="submit">Terima</button>
                <button class="btn btn-danger" type="submit">Tolak</button>
            </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection