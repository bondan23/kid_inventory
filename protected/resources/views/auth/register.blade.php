@extends('template')

@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pendaftaran Akun
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pendaftaran Akun</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Form Pendaftaran Akun</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button><!--
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>-->
          </div>
        </div>
        <div class="box-body">
              <div class="box-body">
               <form class="form-horizontal" role="form" method="POST" action="<?php echo url('/register') ?>">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">

                      <div class="form-group has-feedback">
                       <label for="">Data User</label>
                       <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Fullname">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                      </div>
                      <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="nik" value="{{ old('nik') }}" placeholder="NIK">
                        <span class="glyphicon glyphicon-pencil form-control-feedback"></span>
                      </div>
                      <div class="form-group has-feedback">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                      </div>
                      <div class="form-group has-feedback">
                        <label>Bagian</label>
                        <select class="form-control" name="bagian_id" id="pilihbagian">
                            <option value="0">Pilih Bagian</option>
                            @foreach($bagian as $v)
                                <option value="{{$v->id}}">{{$v->nama_bagian}}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="form-group has-feedback">
                        <label>Jabatan</label>
                        <select class="form-control" name="jabatan_id" id="pilihjabatan">
                            <option value="0">Pilih Bagian Dahulu</option>
                        </select>
                      </div>
                      <div class="form-group">
                          <label>Hak Akses</label>
                          <select class="form-control" name="level_id">
                            <option value="1">Admin</option>
                            <option value="2" selected="true">User</option>
                          </select>
                      </div>
                      <div class="form-group">
                        <label>Email Atasan</label>
                        <input type="email" class="form-control" name="email_atasan" value="{{ old('email_atasan') }}" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                      </div>
                      <div class="form-group has-feedback">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                      </div>
                      <div class="form-group has-feedback">
                        <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                      </div>
                      <div class="row">
                        <div class="col-xs-8">
                          <!--<div class="checkbox icheck">
                            <label>
                              <input type="checkbox"> I agree to the <a href="#">terms</a>
                            </label>
                          </div>-->
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                        </div>
                        <!-- /.col -->
                      </div>
                  </form>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                
              </div>
        </div>
        <!-- /.box-body -->
        <!--<div class="box-footer">
          Footer
        </div>-->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection