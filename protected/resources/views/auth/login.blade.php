@extends('login_temp')

@section('content')

<div class="login-box">
  <div class="login-logo">
    <a href="index2.html"><b>Login Page</b></a>
  </div>
  <!-- /.login-logo -->
  @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
					
  <div class="login-box-body">
    <p class="login-box-msg">KID - Sistem.Inf Inventaris Barang</p>

    <form class="form-horizontal" role="form" method="POST" action="{{ url('login')}}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="email" value="{{ old('login') }}" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" namer="remember"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
	<a href="{{ URL::to('/check') }}">Check Item ?</a><br>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@endsection