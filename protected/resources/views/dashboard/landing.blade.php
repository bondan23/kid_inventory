@extends('template')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Welcome to
        <small>KID - Sistem Informasi Inventory</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Home</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <a href="{{ url('form/create') }}">
            <div class="col-lg-4 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>OUT</h3>

                  <p>Barang Keluar</p>
                </div>
                <div class="icon">
                  <i class="ion ion-log-out"></i>
                </div>
              </div>
            </div>
        </a>
        <!-- ./col -->
        <a href="{{ url('check') }}">
            <div class="col-lg-4 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>CHECK</h3>

                  <p>Check Barang</p>
                </div>
                <div class="icon">
                  <i class="ion ion-checkmark-circled"></i>
                </div>
              </div>
            </div>
        </a>
        <!-- ./col -->
        <a href="{{ url('in') }}">
            <div class="col-lg-4 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>IN</h3>

                  <p>Barang Masuk</p>
                </div>
                <div class="icon">
                  <i class="ion ion-log-in"></i>
                </div>
              </div>
            </div>
        </a>
        <!-- ./col -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection