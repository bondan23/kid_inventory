<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('assets/dist/img/user2-160x160.jpg')}} " class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ ucfirst(Auth::user()->name) }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <!--<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="{{ url('/dashboard') }}">
            <i class="fa fa-dashboard"></i> <span>DASHBOARD</span>
          </a>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="ion ion-log-out"></i>
            <span>OUT</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <!--<li><a href="{{ url('form') }}"><i class="fa fa-search"></i> Lihat Permohonan</a></li>-->
            <li><a href="{{ url('form/create')}}"><i class="fa fa-pencil"></i> Input Barang Keluar</a></li>
            <li><a href="{{ url('form')}}"><i class="fa fa-search"></i> Lihat Barang Keluar</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="ion ion-checkmark-circled"></i>
            <span>CHECK</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <!--<li><a href="{{ url('form') }}"><i class="fa fa-search"></i> Lihat Permohonan</a></li>-->
            <li><a href="{{ url('check')}}"><i class="fa fa-search"></i> Check Barang Keluar</a></li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="ion ion-log-in"></i>
            <span>IN</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <!--<li><a href="{{ url('form') }}"><i class="fa fa-search"></i> Lihat Permohonan</a></li>-->
            <li><a href="{{ url('in')}}"><i class="fa fa-pencil"></i> Input Barang Masuk</a></li>
          </ul>
        </li>
        
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>USER</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="{{ url('register') }}">
                <i class="fa fa-user-plus"></i>
                <span>Register User</span>
              </a>
            </li>
            <li>
              <a href="{{ url('listuser') }}">
                <i class="fa fa-users"></i>
                <span>List of User</span>
              </a>
            </li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gear"></i>
            <span>MASTER</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
           <li>
              <a href="#"><i class="fa fa-circle-o"></i> Bagian <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li>
                  <a href="{{ url('bagian') }}">
                    <i class="fa fa-search"></i>
                    <span>Lihat Bagian</span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('bagian/create') }}">
                    <i class="fa fa-plus"></i>
                    <span>Tambah Bagian</span>
                  </a>
                </li>
              </ul>
            </li> 
            <li>
              <a href="#"><i class="fa fa-circle-o"></i> Jabatan <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li>
                  <a href="{{ url('jabatan') }}">
                    <i class="fa fa-search"></i>
                    <span>Lihat Jabatan</span>
                  </a>
                </li>
                <li>
                  <a href="{{ url('jabatan/create') }}">
                    <i class="fa fa-plus"></i>
                    <span>Tambah Jabatan</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>REPORT</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="{{ url('report/monthly') }}">
                <i class="fa fa-search"></i>
                <span>Monthly Report</span>
              </a>
            </li>
            <li>
              <a href="{{ url('report/daily') }}">
                <i class="fa fa-search"></i>
                <span>Daily Report</span>
              </a>
            </li>
          </ul>
        </li>

       <li class="treeview">
          <a href="#">
            <i class="fa fa-archive"></i>
            <span>BARANG</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="{{ url('barang') }}">
                <i class="fa fa-search"></i>
                <span>Lihat Barang</span>
              </a>
            </li>
            <li>
              <a href="{{ url('barang/create') }}">
                <i class="fa fa-barcode"></i>
                <span>Barcode Generator</span>
              </a>
            </li>
          </ul>
        </li>
        
        @if(Auth::user()->level_id == 10)
            <li>
              <a href="{{ url('verifikasi') }}">
                <i class="fa fa-files-o"></i>
                <span>Permintaan Verifikasi</span>
                <span class="label label-primary pull-right">{{ $count }}</span>
              </a>
            </li>
            <li>
              <a href="{{ url('register') }}">
                <i class="fa fa-user"></i>
                <span>Pendaftaran User</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i>
                <span>Master Data</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('bagian/create') }}"><i class="fa fa-pencil"></i> Master Bagian</a></li>
                <li><a href="{{ url('jabatan/create')}}"><i class="fa fa-pencil"></i> Master Jabatan</a></li>
              </ul>
            </li>
        @endif
        <!--<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="label label-primary pull-right">4</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="../layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li>-->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li>
              <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li>
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>