@extends('template')

@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Barang Masuk
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pendaftaran Akun</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Form Input Barang Masuk</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button><!--
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>-->
          </div>
        </div>
        <div class="box-body">
              <div class="box-body">
                   <input style="text-align:center;" type="text" class="form-control barcodefield_be" name="password" placeholder="Scan or Type Barcode...">
                   <div class="text-center loading" style="margin-top:5px;">
                        Loading <i class="fa fa-spinner fa-spin fa-fw"></i>
                    </div>
                    <p style="margin-top:10px;font-weight:bold;text-align:center;" class="validField"></p>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                
              </div>
        </div>
        <!-- /.box-body -->
        <!--<div class="box-footer">
          Footer
        </div>-->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
