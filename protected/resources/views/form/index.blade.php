@extends('template')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Barang Keluar
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Barang Keluar</h3>

              <div class="box-tools">
                <div style="width: 150px;" class="input-group input-group-sm">
                  <input type="text" placeholder="Search" class="form-control pull-right" name="table_search">

                  <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>User</th>
                  <th>Tipe Perangkat</th>
                  <th>Nama Perangkat</th>
                  <th>Tanggal Peminjaman</th>
                  <th>Tujuan</th>
                  <th>Lama Peminjaman</th>
                  <th>Status</th>
                </tr>
                <tbody>
                <?php $no=1; ?>
                @foreach($data as $v)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $v->user->name }}</td>
                  <td>{{ $v->tipe_perangkat }}</td>
                  <td>{{ $v->barang->nama_barang }}</td>
                  <td>{{ $v->tanggal }}</td>
                  <td>
                      {{$v->tujuan}}
                  </td>
                  <td>
                      {{$v->lama_pinjaman}} Hari
                  </td>
                  <td>
                    @if($status[$v->id]['status'] == false)
                        <span class="label label-danger">
                            {{ $status[$v->id]['msg'] }}
                        </span>
                    @elseif($status[$v->id]['status'] === 'today')
                        <span class="label label-warning">
                            {{ $status[$v->id]['msg'] }}
                        </span>
                    @else
                        <span class="label label-success">
                            {{ $status[$v->id]['msg'] }}
                        </span>
                    @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
              </table>
            </div>
            <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection