@extends('template')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Barang Keluar
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create Form</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Form Izin Barang Keluar Untuk Perangkat Komputer</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button><!--
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>-->
          </div>
        </div>
        <div class="box-body">
          <form role="form" method="POST" action="<?php echo url('form') ?>">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
                <div class="form-group has-feedback">
                    <label for="">SEARCH BY NIK</label>
                    <input type="text" class="form-control searchbynik" placeholder="Input Nik">
                </div>
                <div class="form-group">
                 <div class="row">
                     <div class="col-md-6">
                         <div class="form-group">
                             <label for="NIK">NIK</label>
                             <input type="text" class="form-control" placeholder="NIK" name="nik" value="{{ old('nik') }}" readonly="true">
                             @if (count($errors->form) > 0)<b class="text-red">{{ $errors->form->first('nik') }}</b>@endif
                         </div>
                         <div class="form-group">
                             <label for="NAMA">Nama</label>
                             <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{ old('nama') }}" readonly="true">
                             @if (count($errors->form) > 0)<b class="text-red">{{ $errors->form->first('nama') }}</b>@endif
                         </div>
                     </div>
                     <div class="col-md-6">
                         <div class="form-group">
                             <label for="bagian">Bagian/Dept</label>
                             <input type="text" class="form-control bagian" placeholder="Bagian/Departement" value="{{ old('bagian') }}" readonly="true">
                             <input type="hidden" class="form-control" name="bagian_id" readonly="true">
                         </div>
                         <div class="form-group">
                             <label for="NAMA">Jabatan</label>
                             <input type="text" class="form-control jabatan" placeholder="Jabatan" value="{{ old('jabatan') }}" readonly="true">
                             <input type="hidden" class="form-control" name="jabatan_id" readonly="true">
                             <input type="hidden" class="form-control" name="user_id" readonly="true">
                             <input type="hidden" class="form-control" name="email_atasan" readonly="true">
                         </div>
                     </div>
                     <div class="col-md-12">
                         <div class="form-group">
                             <label for="type" class="control-label col-md-3">Type Perangkat Komputer</label>
                             <div class="col-md-8">
                                 <label class="checkbox-inline">
                                      <input type="radio" class="defRadio" name="tipe_perangkat" value="notebook" checked> Notebook
                                 </label>
                                 <label class="checkbox-inline">
                                      <input type="radio" class="defRadio" name="tipe_perangkat" value="pc_desktop"> PC Desktop
                                 </label>
                                 <label class="checkbox-inline">
                                      <input type="radio" class="defRadio" name="tipe_perangkat" value="printer"> Printer
                                 </label>
                                 <label class="checkbox-inline">
                                      <input type="radio" class="defRadio" name="tipe_perangkat" value="projector"> Projector
                                 </label>
                                 <label class="checkbox-inline">
                                      <input type="radio" class="defRadio" name="tipe_perangkat" value="flashdisk"> Flashdisk
                                 </label>
                             </div>
                             <div class="col-md-5">
                                 <label class="checkbox-inline">
                                      <input type="radio" name="tipe_perangkat" class="lainnya"> <b class="othersType" style="font-weight:normal">Lainnya</b>
                                 </label>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-12" style="margin-top:15px">
                         <div class="form-group">
                             <label for="type" class="control-label col-md-3">Nama Perangkat Komputer</label>
                             <div class="col-md-8">
                                 <input type="text" name="nama_perangkat" class="form-control namefield" value="{{ old('nama_perangkat') }}">
                                 <input type="hidden" name="barang_id" class="field_idbarang">
                             </div>
                         </div>
                     </div>
                     <div class="col-md-12" style="margin-top:15px">
                        
                         <div class="form-group">
                             <label for="type" class="control-label col-md-3">Barcode Perangkat</label>
                             <div class="col-md-8">
                                 <input type="text" name="barcode" class="form-control barcodefield" value="{{ old('barcode') }}">
                             </div>
                         </div>
                     </div>
                     <div class="col-md-12" style="margin-top:15px;">
                         <div class="form-group col-md-4">
                             <label for="tanggal">Tanggal</label>
                             <input type="text" class="form-control" name="tanggal" id="tanggal">
                         </div>
                         <div class="form-group col-md-6">
                             <label for="lamanya">Lamanya</label>
                             <div class="input-group">
                              <input type="text" class="form-control" name="lama_pinjaman">
                              <div class="input-group-addon">Hari</div>
                            </div>
                         </div>
                     </div>
                     <div class="col-md-12" style="margin-top:15px;">
                         <div class="form-group">
                             <label for="tujuan">Tujuan</label>
                             <textarea class="form-control" name="tujuan"></textarea>
                         </div>
                     </div>
                 </div>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button class="btn btn-primary" type="submit">Submit</button>
              </div>
            </form>
        </div>
        <!-- /.box-body -->
        <!--<div class="box-footer">
          Footer
        </div>-->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection


@section('modal')
    @include('form.modal_lainnya')
@endsection