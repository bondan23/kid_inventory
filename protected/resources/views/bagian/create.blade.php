@extends('template')

@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Bagian
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pendaftaran Akun</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Form Input Bagian</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button><!--
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>-->
          </div>
        </div>
        <div class="box-body">
              <div class="box-body">
               {!! Form::open(array('route' => 'bagian.store', 'method' => 'POST')) !!}

                      <div class="form-group has-feedback">
                       <label for="">Nama Bagian</label>
                       <input type="text" class="form-control" name="nama_bagian" value="{{ old('nama_bagian') }}" placeholder="Nama Bagian">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if (count($errors->form) > 0)<b class="text-red">{{ $errors->form->first('nama_bagian') }}</b>@endif
                      </div>
                      <div class="row">
                        <div class="col-xs-8">
                          <!--<div class="checkbox icheck">
                            <label>
                              <input type="checkbox"> I agree to the <a href="#">terms</a>
                            </label>
                          </div>-->
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                          <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                        </div>
                        <!-- /.col -->
                      </div>
                {!! Form::close() !!}
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                
              </div>
        </div>
        <!-- /.box-body -->
        <!--<div class="box-footer">
          Footer
        </div>-->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection