@extends('template')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       User
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">List User</h3>

              <div class="box-tools">
                <div style="width: 150px;" class="input-group input-group-sm">
                  <input type="text" placeholder="Search Name/NIK..." class="form-control pull-right" name="table_search_user">
                  <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding ajax_user_field">
              <table class="table table-hover">
               <thead>
                   <tr>
                      <th>No</th>
                      <th>NIK</th>
                      <th>Fullname</th>
                      <th>Email</th>
                      <th>Bagian</th>
                      <th>Jabatan</th>
                      <th>Hak Akses</th>
                      <th>Email Atasan</th>
                    </tr>
                </thead>
                <?php $no=1; ?>
                @if(!empty($data))
                <tbody>
                    @foreach($data as $v)
                        <tr>
                            <td>
                                {{ $no++ }}
                            </td>
                            <td>
                                {{ $v->nik }}
                            </td>
                            <td>
                                {{ $v->name }}
                            </td>
                            <td>
                                {{ $v->email }}
                            </td>
                            <td>
                                {{ $v->bagian->nama_bagian }}
                            </td>
                            <td>
                                {{ $v->jabatan->nama_jabatan }}
                            </td>
                            <td>
                               @if($v->level_id == 2)
                                User
                               @endif
                            </td>
                            <td>
                                {{ $v->email_atasan }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                @endif
              </table>
            </div>
            <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <?php echo $data->links(); ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection