<table class="table table-hover">
    <thead>
       <tr>
          <th>No</th>
          <th>NIK</th>
          <th>Fullname</th>
          <th>Email</th>
          <th>Bagian</th>
          <th>Jabatan</th>
          <th>Hak Akses</th>
          <th>Email Atasan</th>
        </tr>
    </thead>
    <?php $no=1; ?>
    @if(!empty($data))
    <tbody>
        @foreach($data as $v)
            <tr>
                <td>
                    {{ $no++ }}
                </td>
                <td>
                    {{ $v->nik }}
                </td>
                <td>
                    {{ $v->name }}
                </td>
                <td>
                    {{ $v->email }}
                </td>
                <td>
                    {{ $v->bagian->nama_bagian }}
                </td>
                <td>
                    {{ $v->jabatan->nama_jabatan }}
                </td>
                <td>
                   @if($v->level_id == 2)
                    User
                   @endif
                </td>
                <td>
                    {{ $v->email_atasan }}
                </td>
            </tr>
        @endforeach
    </tbody>
    @endif
</table>