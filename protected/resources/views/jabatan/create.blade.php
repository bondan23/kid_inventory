@extends('template')

@section('content')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Bagian
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pendaftaran Akun</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Form Pendaftaran Akun</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button><!--
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>-->
          </div>
        </div>
        <div class="box-body">
              <div class="box-body">
               {!! Form::open(array('route' => 'jabatan.store', 'method' => 'POST')) !!}
                     <div class="form-group">
                          <label>Nama Bagian</label>
                          <select class="form-control" name="bagian_id" id="pilihbagian">
                               <option value="0" selected>Pilih Bagian</option>
                            @foreach($bagian as $v)
                                <option value="{{ $v->id }}">{{ $v->nama_bagian }}</option>
                            @endforeach
                          </select>
                          @if (count($errors->form) > 0)<b class="text-red">{{ $errors->form->first('bagian_id') }}</b>@endif
                      </div>
                      <div class="form-group has-feedback">
                       <label for="">Nama Jabatan</label>
                       <input type="text" class="form-control" name="nama_jabatan" value="{{ old('nama_jabatan') }}" placeholder="Nama Jabatan">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        @if (count($errors->form) > 0)<b class="text-red">{{ $errors->form->first('nama_jabatan') }}</b>@endif
                      </div>
                      <div class="row">
                        <div class="col-xs-8">
                          <!--<div class="checkbox icheck">
                            <label>
                              <input type="checkbox"> I agree to the <a href="#">terms</a>
                            </label>
                          </div>-->
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                          <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                        </div>
                        <!-- /.col -->
                      </div>
                {!! Form::close() !!}
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                
              </div>
        </div>
        <!-- /.box-body -->
        <!--<div class="box-footer">
          Footer
        </div>-->
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection