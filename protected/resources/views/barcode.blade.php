<?php /*
$filename = "letter.doc";
header("Content-type: application/vnd.ms-word");
header("Content-Disposition: attachment; filename=" . $filename);*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <div class="container text-center" style="border: 1px solid #a1a1a1;padding: 15px;width: 320px;">
	
        <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('23081993', 'C39E')}}" alt="barcode" />
        <br>
        <center>23081993</center>
    </div>
    <br>
    <div class="container text-center" style="border: 1px solid #a1a1a1;padding: 15px;width: 304px;">
	
        <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('*37428/ASUS/2014*', 'C39',1,33)}}" alt="barcode" />
        <br>
        <center>37428/ASUS/2014</center>
    </div>
    <br>
    <div class="container text-center" style="border: 1px solid #a1a1a1;padding: 15px;width: 222px;">
	
        <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('*37428/ASUS/2014*', 'C128',1,33)}}" alt="barcode" />
        <br>
        <center>37428/ASUS/2014</center>
    </div>
    
</body>
</html>