@extends('login_temp')

@section('content')

<div class="login-box">
  <div class="login-logo">
    <a href="index2.html"><b>Checking Item</b></a>
  </div>
  <div class="login-box-body">
    <p class="login-box-msg">KID - Sistem.Inf Inventaris Barang</p>
    <input style="text-align:center;" type="text" class="form-control barcodefield" name="password" placeholder="Scan or Type Barcode...">
    <p style="margin-top:10px;font-weight:bold;text-align:center;" class="validField"></p>
    <div class="text-center loading" style="margin-top:5px;">
        Loading <i class="fa fa-spinner fa-spin fa-fw"></i>
    </div>
	<a href="{{ URL::to('login') }}" class="btn btn-primary pull-right">Login Here</a><br>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

@endsection