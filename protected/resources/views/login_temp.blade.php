<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIM Inventory | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/AdminLTE.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('assets/plugins/iCheck/square/blue.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">

@yield('content')

<!-- jQuery 2.2.0 -->
<script src="{{ asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('assets/plugins/iCheck/icheck.min.js')}}"></script>

<script src="{{ asset('assets/dist/js/jquery.scannerdetection.js') }}"></script>
<script>
  var link = '{{ url('/') }}/';
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
    
    $('.barcodefield').focus();
    $('.loading').hide();
      
    $('.barcodefield').on('keyup',function(e){
        var val = ($(this).val() != null || $(this).val() != '')?$(this).val():1;
        var len = val.length;
        var clean = "?barcode="+val;
        
        if(len > 7 && e.keyCode != 13)
        {
            $.ajax({
                url:link+'barcode'+clean,
                dataType:'json',
                method:"GET",
                beforeSend: function() {
                    $('.loading').show();
                    $('.validField').hide();
                },
                success: function(res) {
                    if(res.status != false)
                    {
                        var msg = '<i class="fa fa-check"></i> Data Valid <br /> Nama Item : '+res.data.nama_barang+'<br /> Tujuan Peminjaman : '+res.data.tujuan+'<br />Di Pinjam Oleh : '+res.data.borrow_by+'<br /> Status Pengembalian : '+res.data.msg;

                        $('.validField').css('color','green');
                        $('.validField').html(msg);  
                    }
                    else{
                        $('.validField').css('color','red'); 
                        $('.validField').html('<i class="fa fa-warning"></i> Data Not Valid <br> Item Tidak Terdaftar! <br />Silakan Kembali ke IT !');
                    }
                },
                complete: function() {
                    $('.loading').hide();
                    $('.validField').show();
                }
            });
        }
    });
      
    $(document).scannerDetection({
        timeBeforeScanTest: 200, // wait for the next character for upto 200ms
        startChar: [47], // Prefix character for the cabled scanner (OPL6845R)
        endChar: [47,13], // be sure the scan is complete if key 13 (enter) is detected
        avgTimeByChar: 40, // it's not a barcode if a character takes longer than 40ms
        onComplete: function(barcode, qty){ 
            $('.barcodefield').val(barcode);
            console.log(barcode);
            /*$.getJSON(link+'barcode/'+barcode,function(res){
                if(res.status != false)
                {
                    $('.namefield').val(res.data.nama_barang);
                    $('.barcodefield').val(res.data.barcode);
                }
                else{
                    console.log('data belum ada');
                }
            })*/
            //alert(barcode)
        } // main callback function	
    });
    
  });
</script>
</body>
</html>
