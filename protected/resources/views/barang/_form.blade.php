{!! Form::model($barang,array('route'=>$route,'method'=>$method))!!}
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    {!! Form::label('nama_barang', 'Nama Barang') !!}
                    {!! Form::text('nama_barang', old('nama_barang',$barang->nama_barang), ['class' => 'form-control']) !!}
                    @if (count($errors->form) > 0)<b class="text-red">{{ $errors->form->first('nama_barang') }}</b>@endif
                </div>
                <div class="col-md-6">
                    {!! Form::label('barcode', 'Barcode') !!}
                    <div class="input-group">
                        {!! Form::text('barcode', old('barcode',$barang->barcode), ['class' => 'barcodefield form-control','readonly'=>'true']) !!}
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-target="barcodefield" onclick="generate_barcode(this)">Barcode Generator</button>
                        </span>
                    </div><!-- /input-group -->
                    
                         @if (count($errors->form) > 0)<b class="text-red">{{ $errors->form->first('barcode') }}</b>@endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- /.col -->
        <div class="col-md-4 col-md-offset-8">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
        </div>
        <!-- /.col -->
    </div>
{!! Form::close() !!}