<table class="table table-hover">
    <thead>
       <tr>
          <th>No</th>
          <th>Nama Barang</th>
          <th>Barcode</th>
          <th>Aksi</th>
        </tr>
    </thead>
    <?php $no=1; ?>
    @if(!empty($data))
    <tbody>
        @foreach($data as $v)
            <tr>
                <td>
                    {{ $no++ }}
                </td>
                <td>
                    {{ $v->nama_barang }}
                </td>
                <td>
                    {{ $v->barcode }}
                </td>
                <td width=200>
                    <a href="{{ route('barang.edit',$v->id) }}" class="btn btn-primary">
                        <i class="fa fa-pencil"></i> Edit
                    </a>
                    &nbsp;
                    <a href="javascript:void(0)" onclick="hapus_modal(this)" data-id="{{ $v->id }}"  class="btn btn-danger" data-hapus="barang" data-route="barang/{{$v->id}}" data-toggle="modal" data-target="#confirmModal">
                        <i class="fa fa-trash"></i> Delete
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
    @endif
</table>

<?php echo $data->links(); ?>