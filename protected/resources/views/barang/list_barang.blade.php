@extends('template')


@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Barang
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Barang</h3>

              <div class="box-tools">
                <div style="width: 250px;" class="input-group input-group-sm">
                  <input type="text" placeholder="Search Barcode/Nama Barang..." class="form-control pull-right" name="table_search_barang">
                  <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding ajax_barang_field">
              <table class="table table-hover">
               <thead>
                   <tr>
                      <th>No</th>
                      <th>Nama Barang</th>
                      <th>Barcode</th>
                      <th>Aksi</th>
                    </tr>
                </thead>
                <?php $no=1; ?>
                @if(!empty($data))
                <tbody>
                    @foreach($data as $v)
                        <tr>
                            <td>
                                {{ $no++ }}
                            </td>
                            <td>
                                {{ $v->nama_barang }}
                            </td>
                            <td>
                                {{ $v->barcode }}
                            </td>
                            <td width=200>
                                <a href="{{ route('barang.edit',$v->id) }}" class="btn btn-primary">
                                    <i class="fa fa-pencil"></i> Edit
                                </a>
                                &nbsp;
                                <a href="javascript:void(0)" onclick="hapus_modal(this)" data-id="{{ $v->id }}"  class="btn btn-danger" data-hapus="barang" data-route="barang/{{$v->id}}" data-toggle="modal" data-target="#confirmModal">
                                    <i class="fa fa-trash"></i> Delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                @endif
              </table>
              
            <?php echo $data->links(); ?>
            </div>
            <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection


@section('modal')
    @include('modal_confirm',['modal_title'=>'Barang'])
@endsection