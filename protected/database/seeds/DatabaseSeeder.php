<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*factory(App\User::class,1)->create();*/
        factory(App\Bagian::class,1)->create();
        factory(App\Jabatan::class,1)->create();
        $this->call(UserSeeder::class);
        // $this->call(UsersTableSeeder::class);
    }
}
