<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        
        $user = array(
            array(
                'name' => 'Administrator',
                'email' => 'admin@admin.com',
                'email_atasan' => 'admin@admin.com',
                'nik'=>'000000',
                'bagian_id'=>null,
                'jabatan_id'=>null,
                'level_id'=>1,
                'password' => bcrypt('admin123'),
                'remember_token' => str_random(10),
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ),
            array(
                'name' => 'Bondan Eko Prasetyo',
                'email' => 'bondan.ekoprasetyo@gmail.com',
                'email_atasan' => 'bondan.ekoprasetyo@yahoo.com',
                'nik'=>'51411525',
                'bagian_id'=>1,
                'jabatan_id'=>1,
                'level_id'=>2,
                'password' => bcrypt('admin123'),
                'remember_token' =>null,
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            )
        );
        
        DB::table('users')->insert($user);
    }
}
