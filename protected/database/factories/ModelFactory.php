<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});*/

/*$factory->define(App\User::class, function () {
    return [
        'name' => 'Administrator',
        'email' => 'admin@admin.com',
        'email_atasan' => 'admin@admin.com',
        'nik'=>'000000',
        'jabatan_id'=>null,
        'level_id'=>1,
        'password' => bcrypt('admin123'),
        'remember_token' => str_random(10),
    ];
});*/

$factory->define(App\Bagian::class, function () {
    return [
        'nama_bagian'=>'IT',
    ];
});

$factory->define(App\Jabatan::class, function () {
    return [
        'bagian_id'=>'1',
        'nama_jabatan'=>'Staff Programmer'
    ];
});
