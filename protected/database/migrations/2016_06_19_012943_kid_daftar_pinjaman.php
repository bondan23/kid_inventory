<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KidDaftarPinjaman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftar_pinjaman', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jabatan_id');
            $table->integer('bagian_id');
            $table->integer('barang_id')->unsigned();
            $table->integer('user_id');
            $table->string('tipe_perangkat');
            $table->string('tanggal',10);
            $table->mediumInteger('lama_pinjaman');
            $table->text('tujuan');
            $table->tinyInteger('status');
            $table->timestamp('due_date')->nullable()->default(null);
            $table->timestamps();
            
            $table->foreign('barang_id')
              ->references('id')->on('daftar_barang')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('daftar_pinjaman');
    }
}
