<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterJabatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('jabatan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bagian_id')->unsigned();
            $table->string('nama_jabatan',255);
            $table->timestamps();
            
            $table->foreign('bagian_id')
              ->references('id')->on('bagian')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('jabatan');
    }
}
