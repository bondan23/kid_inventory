<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('jabatan_id')->nullable();
            $table->tinyInteger('bagian_id')->nullable();
            $table->tinyInteger('level_id');
            $table->string('name');
            $table->string('nik')->unique();
            $table->string('email')->unique();
            $table->string('email_atasan');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
