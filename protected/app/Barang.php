<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = "daftar_barang";
    
    protected $fillable = ['nama_barang','barcode','user_id'];
    
    protected $hidden = ['created_at','updated_at'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function daftar()
    {
        return $this->hasMany('App\DaftarPinjaman','barang_id','id');
    }
}
