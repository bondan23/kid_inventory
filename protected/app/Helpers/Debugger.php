<?php

if (!function_exists('debugger')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function debugger($txt)
    {
        echo "<pre>";
        print_r($txt);
        echo "</pre>";
    }
}
