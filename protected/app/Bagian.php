<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bagian extends Model
{
    protected $table = "bagian";
    
    protected $fillable = [
        'nama_bagian'
    ];
    
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    
    public function jabatan()
    {
        return $this->hasMany('App\Jabatan');
    }
}
