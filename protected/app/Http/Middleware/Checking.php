<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use App;

class Checking
{
    
    public function handle($request, Closure $next)
    {
        //jika bukan admin maka tidak bisa mengakses url /blog/create
        if ( Auth::user() == null)
        {
            return redirect('login');
        }
        else{
            if (Auth::user()->level_id == 2) {
                App::abort('404');
            }
        }

        return $next($request);
    }
    
}