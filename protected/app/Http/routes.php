<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('login/{type?}/{other?}', 'Auth\AuthController@getLogin');
Route::post('login/{type?}/{other?}', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@logout');

Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@postRegister');

Route::get('setting','Auth\Authcontroller@resetPass');

Route::controllers([
	'password' => 'Auth\PasswordController',
]);

Route::post('resetpass','Auth\PasswordController@customReset');

Route::get('bondan',function(){
    abort(404,'Error');
});

Route::get('check','CheckController@index');
Route::resource('verifikasi','VerifikasiController');

/*RESOURCE*/
Route::group(['middleware' => 'auth'], function () {
    Route::resource('bagian','BagianController');
    Route::resource('jabatan','JabatanController');
    Route::resource('barang','BarangController');
    Route::resource('in','InController');
    Route::resource('form','FormController');
});

Route::get('listuser','UserController@index');
Route::get('barcodelist','DashboardController@barcode');
Route::get('/','CheckController@index');
Route::get('dashboard','DashboardController@index');

/*AJAX HANDLE*/
Route::get('getjabatan/{id}','JabatanController@getData');
Route::get('getdatapegawai/{nik}','FormController@getDataPegawai');
Route::get('barcode/{tipe?}','BarangController@barcodeScan');
Route::get('test','FormController@test');
Route::get('barcode_out','BarangController@getBarcode');
Route::post('search_user','UserController@search_user');
Route::post('search_barang','BarangController@search_barang');
/*AJAX HANDLE*/

/*TOKEN REGENERATE*/
Route::get('regenerate',function(){
    Session::regenerateToken();
    return Session::token();
});


Route::get('generate','BarangController@generateBarcode');
/*TOKEN REGENERATE*/

/*REPORT*/
Route::get('report/monthly/{date?}','ReportController@monthly');
Route::get('report/daily/{date?}','ReportController@daily');
/*REPORT*/

Route::get('sending','TestController@send');
