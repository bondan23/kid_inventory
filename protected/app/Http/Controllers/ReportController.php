<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon as Carbon;
use App\DaftarPinjaman as Daftar;
use DB;

class ReportController extends Controller
{
    public function monthly(Request $request,$date=null)
    {
        if($date == null)
        {
            $now = Carbon::today();
            $month = $now->month;
            $year = $now->year;
        }
        else{
            $now = Carbon::today();
            $exp = explode("-",$date);
            $month = $exp[0];
            $year = $exp[1];
        }
        
       $data = Daftar::where(DB::raw('EXTRACT(MONTH FROM tanggal)'),$month)->where(DB::raw('EXTRACT(YEAR FROM tanggal)'),$year)->get();
        
        
        foreach($data as $v)
        {
            //make instance for carbon
            $created = new Carbon(substr($v->due_date,0,10));
            $diff = $created->diffInDays($now,false); 
            /*$nows = new Carbon('2016-06-11');
            $duedate = new Carbon('2016-06-10');
            $get = $duedate->diffInDays($nows,false);
            echo $get."<br>";*/
            $test = $created->diff($now);
            /*debugger($test);
            echo $diff."<br>";*/
            if($diff > 0)
            {
                if($diff > 1)
                {
                    $day = "days";
                }
                else{
                    $day = "day";
                }
                
                $status_item[$v->id] = array(
                    "status"=>false,
                    "msg"=>"Late for $diff $day"
                );
            }
            else if($diff == 0)
            {
                $status_item[$v->id] = array(
                    "status"=>'today',
                    "msg"=>"This day is the last day"
                );
            }
            else{
                $status_item[$v->id] = array(
                    "status"=>true,
                    "msg"=>"Valid"
                );
            }
        }
        
        $arr_month = ['January','February','March','April','May','June','July','August','September','October','November','Desember'];
        
        $this->data['month'] = $arr_month[$month-1];
        $this->data['year'] = $year;
        $this->data['data'] = $data;
        $this->data['status'] = @$status_item;
        
        if($request->ajax())
        {
            return view('report.monthly_ajax',$this->data);
        }
        else{
            return view('report.monthly',$this->data);
        }
    }
    
    public function daily(Request $request,$date=null)
    {
        if($date == null)
        {
            $now = Carbon::today();
            $month = $now->month;
            $year = $now->year;
            $day = $now->day;
        }
        else{
            $now = Carbon::today();
            $exp = explode("-",$date);
            $year = $exp[0];
            $month = $exp[1];
            $day = $exp[2];
        }
        
       $data = Daftar::where(DB::raw('EXTRACT(MONTH FROM tanggal)'),$month)->where(DB::raw('EXTRACT(YEAR FROM tanggal)'),$year)->where(DB::raw('EXTRACT(DAY FROM tanggal)'),$day)->get();
        
        
        foreach($data as $v)
        {
            //make instance for carbon
            $created = new Carbon(substr($v->due_date,0,10));
            $diff = $created->diffInDays($now,false); 
            /*$nows = new Carbon('2016-06-11');
            $duedate = new Carbon('2016-06-10');
            $get = $duedate->diffInDays($nows,false);
            echo $get."<br>";*/
            $test = $created->diff($now);
            /*debugger($test);
            echo $diff."<br>";*/
            if($diff > 0)
            {
                if($diff > 1)
                {
                    $day = "days";
                }
                else{
                    $day = "day";
                }
                
                $status_item[$v->id] = array(
                    "status"=>false,
                    "msg"=>"Late for $diff $day"
                );
            }
            else if($diff == 0)
            {
                $status_item[$v->id] = array(
                    "status"=>'today',
                    "msg"=>"This day is the last day"
                );
            }
            else{
                $status_item[$v->id] = array(
                    "status"=>true,
                    "msg"=>"Valid"
                );
            }
        }
        
        $arr_month = ['January','February','March','April','May','June','July','August','September','October','November','Desember'];
        
        $this->data['day'] = $day;
        $this->data['month'] = $arr_month[$month-1];
        $this->data['year'] = $year;
        $this->data['data'] = $data;
        $this->data['status'] = @$status_item;
        
        if($request->ajax())
        {
            return view('report.daily_ajax',$this->data);
        }
        else{
            return view('report.daily',$this->data);
        }
    }
}
