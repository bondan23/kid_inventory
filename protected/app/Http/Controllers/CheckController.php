<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class CheckController extends Controller
{
    public function index()
    {
        if(Auth::user() == '')
        {
            return view('check.front_end');
        }
        else{
            return view('check.back_end',$this->data);
        }
    }
}
