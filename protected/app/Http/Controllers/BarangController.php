<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Barang;
use App\DaftarPinjaman as Daftar;
use Carbon\Carbon as Carbon;
use Input;
use App;
use DB;
use Bonds23\Alphanumnique\Alphanumnique;
use Validator;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data['data'] = Barang::paginate(20);
            return view('barang.list_barang_ajax',$data);
        }
        else{
            $this->data['data'] = Barang::paginate(20);
            return view('barang.list_barang',$this->data); 
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['barang'] = new Barang;
        return view('barang.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'Kolom ":attribute" tidak boleh kosong !'
        ];
        
        /*$validator = $this->validate($request,[
           'nama_barang'=>'required',
           'barcode'=>'required',
        ]);*/
        
        $validator = Validator::make($request->all(),[
           'nama_barang'=>'required',
           'barcode'=>'required',
        ],$messages);
        
        if($validator->fails()){
            return redirect('barang/create')
                ->withErrors($validator,'form')
                ->withInput();
        }
        else{
            $post = array_except($request->input(),['_token']);
            
            $create = Barang::create($post);

            if($create)
            {
                return redirect('barang');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['barang'] = Barang::find($id);
        return view('barang.edit',$this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = array_except($request->input(),['_method','_token']);
        $barang = Barang::find($id);
        $barang->nama_barang = $request->input('nama_barang');
        $barang->barcode = $request->input('barcode');
        $create = $barang->save();
        
        if($create)
        {
            return redirect('barang');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = Barang::find($id);
        $barang->delete();
    }
    
    /*AJAX HANDLER*/
    public function generateBarcode()
    {
        // Create new instance of generator class.
        $pattern = str_shuffle("123456789");
        $generator = new Alphanumnique($pattern);

        // Set token length.
        $tokenLength = 10;

        // Call method to generate random string.
        $token = $generator->generate($tokenLength);
        $prefix = "KID";
        $result = $prefix.$token;
        
        echo json_encode(array("token"=>$result));
        
    }
    
    public function getBarcode(Request $request)
    {
        /*if($request->ajax())
        {*/
            $text = Input::get('barcode');
            $data = Barang::where('barcode',$text)->with('user')->get();
            
            if($text != null)
            {
                if(!empty($data))
                {
                    foreach($data as $v)
                    {
                        $result = array(
                            "id"=>$v->id,
                            "nama_barang"=>$v->nama_barang,
                            "barcode"=>$v->barcode,
                            "borrow_by"=>(@$v->user->name)?$v->user->name:"kosong",
                            "status"=>true,
                            "msg"=>"Valid"
                        );
                    }
                }
                else{
                    $result = null;
                }

                if(empty($result))
                {
                    echo json_encode(array(
                        "status"=>false,
                        "data"=>null
                    ));
                }
                else{
                    echo json_encode(array(
                        "status"=>true,
                        "data"=>$result
                    ));
                }
            }else{
                echo json_encode(array(
                    "status"=>false,
                    "data"=>null
                ));
            }
       /* }
        else{
            App::abort(404);
        }*/
    }
    
    public function barcodeScan(Request $request,$tipe="out")
    {
        if($request->ajax())
        {
            $text = Input::get('barcode');

            if($text != null)
            {
                $whereclause = array(
                    array('barcode',$text),
                    array('user_id',"!=",null)
                );

                $data = Barang::where($whereclause)->with('user','daftar')->get();

                $now = Carbon::today();
                if(!empty($data))
                {
                    foreach($data as $v)
                    {
                        foreach($v->daftar as $value)
                        {
                        $created = new Carbon(substr($value->due_date,0,10));
                        $diff = $created->diffInDays($now,false);
                            
                            
                        if($diff > 0)
                        {
                            if($diff > 1)
                            {
                                $day = "days";
                            }
                            else{
                                $day = "day";
                            }
                            
                            $result = array(
                                "id"=>$v->id,
                                "nama_barang"=>$v->nama_barang,
                                "tujuan"=>$value->tujuan,
                                "barcode"=>$v->barcode,
                                "borrow_by"=>$v->user->name,
                                "status"=>false,
                                "msg"=>"<b class='text-danger'>Late for $diff $day</b>"
                            );
                        }
                        else{
                            if(substr($diff,1) > 1)
                            {
                                $day = "days";
                            }
                            else{
                                $day = "day";
                            }
                            
                            if($diff == 0)
                            {
                                $days = "Ini Hari Terakhir Pengembalian";
                            }
                            else{
                                $days = substr($diff,1)." Hari Lagi ";//.$day;
                            }
                            $result = array(
                                "id"=>$v->id,
                                "nama_barang"=>$v->nama_barang,
                                "tujuan"=>$value->tujuan,
                                "barcode"=>$v->barcode,
                                "borrow_by"=>$v->user->name,
                                "status"=>true,
                                "msg"=>"$days"
                            );
                        }


                            if($tipe == "in")
                            {
                                $update = Barang::find($v->id);
                                $update->user_id = null;
                                $update->save();

                                $daftar = Daftar::find($value->id);
                                $daftar->status = 0;
                                $daftar->save();
                            }
                        }
                    }
                }
                else{
                    $result = null;
                }

                if(empty($result))
                {
                    echo json_encode(array(
                    "status"=>false,
                    "data"=>null
                    ));
                }
                else{
                    echo json_encode(array(
                    "status"=>true,
                    "data"=>$result
                    ));
                }
            }
            else{
                echo json_encode(array(
                "status"=>false,
                "data"=>null
                ));
            }
        }
        else{
            App::abort(404);
        }
    }
    
    public function search_barang(Request $request)
    {
        $q = $request->input('q');
        
        $search = Barang::where('barcode','LIKE','%'.$q.'%')->orWhere('nama_barang','LIKE','%'.$q.'%')->paginate(20);
        $data['data'] = $search;
        
        return view('barang.list_barang_ajax',$data);
    }
    
    /*AJAX HANDLER*/
}
