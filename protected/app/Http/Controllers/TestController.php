<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;
use Exception;

class TestController extends Controller
{
    public function send()
    {
        $mail1 = 'friska@keihin.co.id';
        $mail2 = 'bondan.ekoprasetyo@nindyakarya.co.id';
        try{
            $sent = Mail::send('emails.blank', array('key' => 'value'), function($message)
            {
               $message->to('bondan.ekoprasetyo@gmail.com')->subject('A simple test');
            });
            
            if(!$sent)
            {
                throw new Exception('Failed to send');
            }
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }
}
