<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Bagian;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    protected $redirectPath = '/dashboard';
    protected $redirectAfterLogout = '/check';
    
    protected $loginPath = 'login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        if(Auth::user() == ''){
            $this->middleware('auth',['only'=>['getRegister','postRegister','resetPass']]);
            $this->middleware($this->guestMiddleware(), ['except' => ['logout']]);
        }
        else{
           // $this->middleware($this->guestMiddleware(), ['except' => ['logout','getRegister','postRegister']]);
            //$this->middleware('auth');
            $this->middleware('checking',['except'=>'logout']);
        }
    }
    
    
    public function showRegistrationForm()
    {
        $bagian = Bagian::get();
        $this->data['bagian'] = $bagian;
        return view('auth.register',$this->data);
    }
    
    public function getLogin()
    {
        if(Auth::user() == '')
        {
            return $this->showLoginForm();
        }else{
            return redirect('dashboard');
        }
    }
    
    public function resetPass()
    {
        return view('auth.reset');
    }
    
    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        //Auth::guard($this->getGuard())->login($this->create($request->all()));
        $this->create($request->all());
        return redirect('listuser');
    }
    
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'nik' => 'required|min:6|unique:users',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'nik'=> $data['nik'],
            'jabatan_id'=>$data['jabatan_id'],
            'bagian_id'=>$data['bagian_id'],
            'level_id'=>$data['level_id'],
            'email_atasan'=>$data['email_atasan']
        ]);
    }
}
