<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\DaftarPinjaman;


use \Milon\Barcode\DNS1D;
use \Milon\Barcode\DNS2D;

class DashboardController extends Controller
{
    /*public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }*/
    
    public function index()
    {
        return view('dashboard.landing',$this->data);
    }
    
    public function barcode()
    {
        return view('barcode');
    }
}
