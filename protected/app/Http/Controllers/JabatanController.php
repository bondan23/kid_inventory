<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Jabatan;
use App\Bagian;
use Validator;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $this->data['data'] = Jabatan::orderBy('bagian_id', 'ASC')->paginate(20);
        
       if($request->ajax())
       {
           return view('jabatan.list_jabatan_ajax',$this->data);
       }
       else{
           return view('jabatan.list_jabatan',$this->data);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bagian = Bagian::get();
        $this->data['bagian'] = $bagian;
        
        return view('jabatan.create',$this->data);
       /* $id=1;
        $a = Jabatan::where('id', $id)->with('bagian')->get();
        debugger($a);*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $post = array_except($request->input(),'_token');
        
         $messages = [
            'required' => 'Kolom ":attribute" tidak boleh kosong !',
        ];
        
        $validator = Validator::make($request->all(),[
           'nama_jabatan'=>'required',
           'bagian_id'=>'min:1'
        ],$messages);
        
        if($validator->fails()){
            return redirect('jabatan/create')
                ->withErrors($validator,'form')
                ->withInput();
        }
        else{
            $create = Jabatan::create($post);
            
            if($create)
            {
                return redirect('jabatan');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jabatan = Jabatan::find($id);
        $jabatan->delete();
    }
    
    
    /*AJAX HANDLER*/
    public function getData($id)
    {
        $data = Jabatan::where('bagian_id', $id)->get()->toJson();
        echo $data;
    }
    /*AJAX HANDLER*/
}
