<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Session;

class UserController extends Controller
{
    public function index()
    {
        $this->data['data'] = User::where('level_id',2)->with('bagian.jabatan')->paginate(20);
        return view('user.list',$this->data);
    }
    
    /*AJAX HANDLE*/
    public function search_user(Request $request)
    {
        $q = $request->input('q');
        
        $search = User::where('nik','LIKE','%'.$q.'%')->orWhere('name','LIKE','%'.$q.'%')->get();
        $data['data'] = $search;
        
        return view('user.ajax_list',$data);
    }
    /*AJAX HANDLE*/
}
