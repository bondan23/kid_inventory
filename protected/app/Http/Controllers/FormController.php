<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

/*MODEL*/
use App\DaftarPinjaman;
use App\User;
use App\Barang;
/*MODEL*/

use Validator;
use Auth;
use Faker\Factory as Faker;
use Carbon\Carbon as Carbon;
use Mail;
use Exception;

class FormController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nik = Auth::user()->nik;
        $data = DaftarPinjaman::where('status',1)->get();
        $now = Carbon::today();
        
        foreach($data as $v)
        {
            //make instance for carbon
            $created = new Carbon(substr($v->due_date,0,10));
            $diff = $created->diffInDays($now,false); 
            /*$nows = new Carbon('2016-06-11');
            $duedate = new Carbon('2016-06-10');
            $get = $duedate->diffInDays($nows,false);
            echo $get."<br>";*/
            $test = $created->diff($now);
            /*debugger($test);
            echo $diff."<br>";*/
            if($diff > 0)
            {
                if($diff > 1)
                {
                    $day = "days";
                }
                else{
                    $day = "day";
                }
                
                $status_item[$v->id] = array(
                    "status"=>false,
                    "msg"=>"Late for $diff $day"
                );
            }
            else if($diff == 0)
            {
                $status_item[$v->id] = array(
                    "status"=>'today',
                    "msg"=>"This day is the last day"
                );
            }
            else{
                $status_item[$v->id] = array(
                    "status"=>true,
                    "msg"=>"Valid"
                );
            }
        }
        
        
        
        $this->data['status'] = @$status_item;
        $this->data['data'] = $data;
        return view('form.index',$this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('form.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'Kolom ":attribute" tidak boleh kosong !',
            'min' => 'Isi dari kolom ":attribute" harus lebih besar dari ":min" !'
        ];
        
        $post = array_except($request->input(),['_token','email_atasan']);
        
        //make due date
        $now = new Carbon($request->input('tanggal'));
        $post['due_date'] = $now->addDay($request->input('lama_pinjaman'));
        
        $validator = Validator::make($request->all(),[
           'nik'=>'required',
           'nama'=>'required',
            /*
           'tipe_perangkat'=>'required',
           'nama_perangkat'=>'required',
           'tanggal'=>'required',
           'lamanya'=>'required',
           'tujuan'=>'required',*/
        ],$messages);
        
        if($validator->fails()){
            return redirect('form/create')
                ->withErrors($validator,'form')
                ->withInput();
        }
        else{
            //check barang baru apa existing
            
            $barang = Barang::firstOrNew([
                'nama_barang'=>$request->input('nama_perangkat'),
                'barcode'=>$request->input('barcode')
            ]);
            $barang->user_id = $request->input('user_id');
            
            if($barang->save())
            {
                $id = $barang->id;
                $save = array(
                    "jabatan_id"=>$post['jabatan_id'],
                    "bagian_id"=>$post['bagian_id'],
                    "barang_id"=>$id,
                    "user_id"=>$post['user_id'],
                    "tipe_perangkat"=>$post['tipe_perangkat'],
                    "tanggal"=>$post['tanggal'],
                    "due_date"=>$post['due_date'],
                    "lama_pinjaman"=>$post['lama_pinjaman'],
                    "tujuan"=>$post['tujuan'],
                    "status"=>1,
                );
                $create = DaftarPinjaman::firstOrCreate($save);
            }
            
            /*if($request->input('barang_id') == '')
            {
                $barang = new Barang;
                $barang->nama_barang = $request->input('nama_perangkat');
                $barang->barcode = $request->input('barcode');
                $barang->user_id = $request->input('user_id');
                if($barang->save())
                {
                    $lastinsertId = $barang->id;
                    
                    unset($post['barang_id']);
                    $post['barang_id'] = $lastinsertId;
                    $post['status'] = 1;
                    $create = DaftarPinjaman::create($post);
                }
            }
            else{
                $barang = Barang::find($request->input('barang_id'));
                $barang->user_id = $request->input('user_id');
                if($barang->save())
                {
                    $post['status'] = 1;
                    $create = DaftarPinjaman::create($post);   
                }
            }*/
            
            if($create)
            {
                return redirect('form');
                /*try{
                    $msg['test'] = "Sorry,Test Only!";
                     
                    $sent = Mail::send('emails.blank',$msg, function($message) use ($request){
                       $message->to($request->input('email_atasan'))->subject('A simple test');
                    });

                    if(!$sent)
                    {
                        throw new Exception('Failed to send');
                    }
                    else{
                         return redirect('form');
                    }
                }
                catch(Exception $e)
                {
                    echo $e->getMessage();
                }*/
            }
                
            
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function test()
    {
        echo "HELLO,";
        $faker = Faker::create('id_ID');
        echo $faker->name;
    }
    
    /*AJAX HANDLER*/
    public function getDataPegawai($nik=null)
    {
        if($nik != null)
        {
        $data = User::where('nik',$nik)->with('bagian.jabatan')->get();
            foreach($data as $v)
            {
                $result = array(
                    "id"=>$v->id,
                    "nama"=>$v->name,
                    "nik"=>$v->nik,
                    "jabatan_id"=>$v->jabatan_id,
                    "jabatan"=>$v->jabatan->nama_jabatan,
                    "bagian_id"=>$v->bagian_id,
                    "bagian"=>$v->bagian->nama_bagian,
                    "email"=>$v->email,
                    "email_atasan"=>$v->email_atasan
                );
            }

            if(empty($result))
            {
                echo json_encode(array(
                    "status"=>false,
                    "data"=>null
                ));
            }
            else{
                echo json_encode(array(
                    "status"=>true,
                    "data"=>$result
                ));
            }
        }
        else{
            echo json_encode(array(
                "status"=>false,
                "data"=>null
            ));
        }
    }
    /*AJAX HANDLER*/
}
