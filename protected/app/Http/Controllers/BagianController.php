<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Bagian;
use Validator;

class BagianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $this->data['data'] = Bagian::paginate(20);
        
       if($request->ajax())
       {
           return view('bagian.list_bagian_ajax',$this->data);
       }
       else{
           return view('bagian.list_bagian',$this->data);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bagian.create',$this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = array_except($request->input(),'_token');
        
         $messages = [
            'required' => 'Kolom ":attribute" tidak boleh kosong !',
        ];
        
        $validator = Validator::make($request->all(),[
           'nama_bagian'=>'required',
        ],$messages);
        
        if($validator->fails()){
            return redirect('bagian/create')
                ->withErrors($validator,'form') //->withErrors($validator) jika tidak ingin menspesific-an error
                ->withInput();
        }
        else{
            $create = Bagian::create($post);
            
            if($create)
            {
                return redirect('bagian');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bagian = Bagian::find($id);
        $bagian->delete();
    }
    
}
