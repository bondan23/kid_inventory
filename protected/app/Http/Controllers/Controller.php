<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use App\DaftarPinjaman;

class Controller extends BaseController
{
    public $count_verifikasi;
    public $data = array();
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
    
    public function __construct()
    {
        $this->get_count();
    }
    
    public function get_count()
    {
         $count = DaftarPinjaman::where('status',1)->count();
         $this->data['count'] = $count;
    }
}
