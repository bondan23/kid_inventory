<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'jabatan';
    
    protected $fillable = [
        "bagian_id",
        "nama_jabatan"
    ];
    
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    
    public function bagian()
    {
        return $this->belongsTo('App\Bagian','bagian_id');
    }
}
