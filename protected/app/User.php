<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','nik','jabatan_id','bagian_id','level_id','email_atasan'
    ];
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_at','updated_at'
    ];
    
    public function bagian()
    {
        return $this->belongsTo('App\Bagian');
    }
    
    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan');
    }
}
