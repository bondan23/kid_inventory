<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DaftarPinjaman extends Model
{
    protected $table = 'daftar_pinjaman';
    
    protected $fillable = [
        'user_id', 'bagian_id','jabatan_id','tipe_perangkat','barang_id','tanggal','lama_pinjaman','tujuan','status','due_date'
    ];
    
    public function barang()
    {
        return $this->belongsTo('App\Barang','barang_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
