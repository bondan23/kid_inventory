/*UTILITY*/
function pad2(number) {
    return (number < 10)? number.substr(1,1) : number;
}
/*UTILITY*/

/*RESOURCE CLOSURE*/
var reload_handler = (function(route){
    $.get(link+route,function(data){
        $('.ajax_'+route+'_field').html(data);
    })
})
/*RESOURCE CLOSURE*/

/*AJAX MODAL HANDLER*/
function hapus_modal(x)
{
    var id = $(x).attr('data-id');
    var route = $(x).attr('data-route');
    var source = $(x).attr('data-hapus');
    var source_class = $('.hapus_'+source);
    
    source_class.attr('data-route',route);
    source_class.attr('data-rest',source);
    console.log(route);
}

function hapus(x)
{
    var route = $(x).attr('data-route');
    var rest = $(x).attr('data-rest');
    $.ajax({
        url:link+route,
        method:"DELETE",
        success:function(data)
        {
            reload_handler(rest);
            $('#confirmModal').modal('hide');
            
        }
    })
}

function set_value_lainnya(source)
{
    var val = $('.'+source).val();
    $('#insertModalLainnya').modal('hide');
    var box = $('input[name=tipe_perangkat]');
    
    box.each(function(e,x){
        if($(x).is(':checked'))
        {
            $(x).attr('value',val);
            $('.othersType').html('Lainnya ['+val+']')
        }
    })
}
/*AJAX MODAL HANDLER*/

/*GENERATE BARCODE*/
function generate_barcode(x)
{
    var target = $(x).data('target');
    $.getJSON(link+'generate',function(res){
        $('.'+target).val(res.token);
    })
}
/*GENERATE BARCODE*/

$('document').ready(function(){
    $('#pilihbagian').on('change',function(){
        var val = $(this).val();
        var html ="";
        $.getJSON(link+'getjabatan/'+val,function(data){
            $.each(data,function(key,val){
                html += "<option value='"+val.id+"'>";
                html += val.nama_jabatan;
                html += "</option>";
            })
            console.log(html);
            $('#pilihjabatan').html(html)
        })
    });
    
    var delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();
    
    /*EXPORT EXCEL*/
    $('.exportExcel').click(function(){
        var filename = $(this).attr('data-title');
       
        $(".table").table2excel({
            exclude: ".noExl",
            name: "Excel Document Name",
            filename: filename,
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true
        });
    })
    /*EXPORT EXCEL*/
    
    
    /*AJAX SEARCH*/
        $('input[name=table_search_user]').on('keyup',function(){
            var val = $(this).val();

            if(val.length > 7)
            {
                delay(function(){
                    $.post(link+"search_user",{q:val},function(data){
                        $(".ajax_user_field").html(data);
                    });
                },200);
            }
        })
        
        $('input[name=table_search_barang]').on('keyup',function(){
            var val = $(this).val();

            if(val.length > 4)
            {
                delay(function(){
                    $.post(link+"search_barang",{q:val},function(data){
                        $(".ajax_barang_field").html(data);
                    });
                },200);
            }
        })
    /*AJAX SEARCH*/
    
    $('.searchbynik').on('keyup',function(e){
        var val = ($(this).val() != null || $(this).val() != '')?$(this).val():1;
        var len = val.length;
        
        var nik = $('input[name=nik]');
        var bagian = $('input[name=bagian_id]');
        var jabatan = $('input[name=jabatan_id]');
        var user_id = $('input[name=user_id]');
        var bagian_label = $('.bagian');
        var jabatan_label = $('.jabatan');
        var nama = $('input[name=nama]');
        var email_atasan = $('input[name=email_atasan]');
        
        if(len > 7 && e.keyCode != 13)
        {
            
            delay(function(){
                $.getJSON(link+'getdatapegawai/'+val,function(res){
                    if(res.status == true)
                    {
                        user_id.val(res.data.id)
                        nik.val(res.data.nik)
                        nama.val(res.data.nama)
                        bagian.val(res.data.bagian_id)
                        jabatan.val(res.data.jabatan_id)
                        email_atasan.val(res.data.email_atasan)

                        /*LABEL*/
                        bagian_label.val(res.data.bagian)
                        jabatan_label.val(res.data.jabatan)

                        $('.barcodefield').focus();
                    }
                    else{
                        user_id.val('')
                        nik.val('')
                        nama.val('')
                        bagian.val('')
                        jabatan.val('')

                        /*LABEL*/
                        bagian_label.val('')
                        jabatan_label.val('')
                    }
                })
            },10);
        }
        else{
            user_id.val('')
            nik.val('')
            nama.val('')
            bagian.val('')
            jabatan.val('')

            /*LABEL*/
            bagian_label.val('')
            jabatan_label.val('')
        }
    })
    
    $('.barcodefield').on('keyup',function(e){
        var val = ($(this).val() != null || $(this).val() != '')?$(this).val():1;
        var len = val.length;
        var clean = "?barcode="+val;
        
        if(len > 7 && e.keyCode != 13)
        {
            delay(function(){
                $.getJSON(link+'barcode_out'+clean,{
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                },function(res){
                    if(res.status != false)
                    {
                        if(res.data.borrow_by != 'kosong')
                        {
                            var msg = res.data.nama_barang+" Sedang Dipinjam Oleh User : "+res.data.borrow_by;
                            $('.namefield').attr('placeholder',msg);
                            $('.barcodefield').val('');
                        }
                        else{
                            $('.field_idbarang').val(res.data.id);
                            $('.namefield').val(res.data.nama_barang);
                            $(this).val(res.data.barcode);
                        }
                    }
                    else{
                        $('.namefield').removeAttr('placeholder');
                        $('.namefield').val('');
                        $('.field_idbarang').val('');
                        console.log('data belum ada');
                    }
                })
            },500);
        }
    })
    
    $('input[name=barcode]').on('keypress',function(a){
       if(a.keyCode == 13)
       {
           return false;
       }
    })
    
    /*iCHeck*/
    $('.defRadio').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
    
    $('.lainnya').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
    
    $('.lainnya').on('ifChecked',function(){
       $('#insertModalLainnya').modal('show');
    })
    /*iCHeck*/
    
    /*DatePicker*/
    $('#tanggal').datepicker({
        format:'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        autoclose: true
    });
    
    $('.bulanOnly').datepicker({
        format: "mm-yyyy",
        startView: 1,
        minViewMode: 1,
        maxViewMode: 2,
        autoclose: true
    }).on('hide',function(){
       var val = $(this).val();
        $.get(link+'report/monthly/'+val,function(data){
            var date = val.split('-');
            var month_arr = ['January','February','March','April','May','June','July','August','September','October','November','Desember']
            
            var month = pad2(date[0]);
            var dates = month_arr[month-1]+" "+date[1];
            var filename ="MonthlyReport_"+month_arr[month-1]+"_"+date[1];
            
            $('.exportExcel').attr('data-title',filename);
            $('.bulanReport').html(dates);
            $('.ajax_report_field').html(data);
        })
    })
    
    $('.dateOnly').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
    }).on('hide',function(){
       var val = $(this).val();
        $.get(link+'report/daily/'+val,function(data){
            var date = val.split('-');
            var month_arr = ['January','February','March','April','May','June','July','August','September','October','November','Desember']
            
            var year  = date[0];
            var month = pad2(date[1]);
            var dates = date[2]+" "+month_arr[month-1]+" "+date[0];
            var filename = "DailyReport_"+date[2]+"_"+month_arr[month-1]+"_"+date[0];
            
            $('.exportExcel').attr('data-title',filename);
            $('.bulanReport').html(dates);
            $('.ajax_report_field').html(data);
        })
    })
    /*DatePicker*/
    
   var pathArray = window.location.pathname.split( '/' );
    
    /*CHECKING SIDE*/
    $('.barcodefield_be').focus();
    
    $('.loading').hide();
    
    $('.barcodefield_be').on('keyup',function(e){
        var val = ($(this).val() != null || $(this).val() != '')?$(this).val():1;
        var len = val.length;
        var thisUri = pathArray[2]; // must be in
        //console.log(e.keyCode);
        
        if(len > 7 && e.keyCode != 13)
        {
            /*var clean = val.replace(/\//g, '');*/
            
            if(thisUri == "in"){
                var clean = "/in?barcode="+val;    
            }
            else{
                var clean = "?barcode="+val;
            }
            
            delay(function(){
                $.ajax({
                    url:link+'barcode'+clean,
                    dataType:'json',
                    method:"GET",
                    beforeSend: function() {
                        $('.loading').show();
                        $('.validField').hide();
                    },
                    success: function(res) {
                        if(res.status != false)
                        {
                            if(thisUri == "in"){
                                var msg = '<i class="fa fa-check"></i> Data Valid <br /> Nama Item : '+res.data.nama_barang+'<br /> Tujuan Peminjaman : '+res.data.tujuan+'<br />Di Pinjam Oleh : '+res.data.borrow_by+'<br /> Status Pengembalian : '+res.data.msg+"<br />Sukses Check In Barang !";
                            }
                            else{
                                var msg = '<i class="fa fa-check"></i> Data Valid <br /> Nama Item : '+res.data.nama_barang+'<br /> Tujuan Peminjaman : '+res.data.tujuan+'<br />Di Pinjam Oleh : '+res.data.borrow_by+'<br /> Status Pengembalian : '+res.data.msg;
                            }

                            $('.validField').css('color','green');
                            $('.validField').html(msg);
                        }
                        else{
                            if(thisUri == "in"){
                                $('.validField').css('color','red'); 
                                $('.validField').html('<i class="fa fa-warning"></i> Barang Tidak Ditemukan di Daftar Pinjaman !');
                            }
                            else{
                                $('.validField').css('color','red'); 
                                $('.validField').html('<i class="fa fa-warning"></i> Data Not Valid <br> Item Tidak Terdaftar! <br />Silakan Kembali ke IT !');
                            }
                        }
                    },
                    complete: function() {
                        $('.loading').hide();
                        $('.validField').show();
                    }
                });
                
               /* $.getJSON(link+'barcode'+clean,function(res){
                    if(res.status != false)
                    {
                        if(thisUri == "in"){
                            var msg = '<i class="fa fa-check"></i> Data Valid <br /> Nama Item : '+res.data.nama_barang+'<br /> Tujuan Peminjaman : '+res.data.tujuan+'<br />Di Pinjam Oleh : '+res.data.borrow_by+'<br /> Status Pengembalian : '+res.data.msg+"<br />Sukses Check In Barang !";
                        }
                        else{
                            var msg = '<i class="fa fa-check"></i> Data Valid <br /> Nama Item : '+res.data.nama_barang+'<br /> Tujuan Peminjaman : '+res.data.tujuan+'<br />Di Pinjam Oleh : '+res.data.borrow_by+'<br /> Status Pengembalian : '+res.data.msg;
                        }

                        $('.validField').css('color','green');
                        $('.validField').html(msg);
                    }
                    else{
                        if(thisUri == "in"){
                            $('.validField').css('color','red'); 
                            $('.validField').html('<i class="fa fa-warning"></i> Barang Tidak Ditemukan di Daftar Pinjaman !');
                        }
                        else{
                            $('.validField').css('color','red'); 
                            $('.validField').html('<i class="fa fa-warning"></i> Data Not Valid <br> Item Tidak Terdaftar! <br />Silakan Kembali ke IT !');
                        }
                    }
                })*/
                
            },350);
            
        }
        else{
            $('.validField').html('');
        }
    });
});